//
//  BZRemoteController.h
//  RobotController
//
//  Created by Mark Yin on 13/03/16.
//  Copyright © 2016 BotZone. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum{
    RC_NavigateButton,
    RC_Gyrosensor,
    RC_NavigateCircle
}RemoteControllerMode;

typedef enum
{
    RC_Forward,
    RC_Backward,
    RC_Left,
    RC_Right
}RemoteControllerDirection;

@interface BZRemoteController : NSObject

@property(nonatomic,assign) RemoteControllerMode mode;
@property(nonatomic,assign,readonly) float x;
@property(nonatomic,assign,readonly) float y;

-(void)inputDirectory:(RemoteControllerDirection)direction;

@end
