//
//  main.m
//  RobotController
//
//  Created by Mark Yin on 13/03/16.
//  Copyright © 2016 BotZone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
